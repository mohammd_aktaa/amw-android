package buckle.com;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import buckle.com.adapters.AdapterCourses;
import buckle.com.adapters.AdapterStudents;
import buckle.com.fragment.StudentDialogFullscreenFragment;
import buckle.com.model.Entries;
import buckle.com.model.StudentModel;
import buckle.com.network.RetrofitBuilder;
import buckle.com.network.Routes;
import buckle.com.network.TokenManager;
import buckle.com.network.UserManager;
import buckle.com.utils.Tools;
import buckle.com.utils.ViewAnimation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuDrawerStudentsActivity extends AppCompatActivity {
    public static final int DIALOG_QUEST_CODE = 300;
    Routes service;
    TokenManager tokenManager;
    int page = 0;
    RelativeLayout container;
    UserManager userManager;
    AlertDialog.Builder builder;
    KProgressHUD dialog;
    SwipeRefreshLayout swipe_refresh;
    NavigationView navigationView;
    private ActionBar actionBar;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private boolean is_account_mode = false;
    private TextView drawer_user_name, drawer_user_email;
    private RecyclerView recyclerView;
    private AdapterCourses mAdapter;
    private NestedScrollView nested_scroll_view;
    private ImageButton bt_toggle_input;
    private Button bt_save_input, bt_hide_input, search_btn;
    private View lyt_expand_input;
    private TextInputEditText fromDate, toDate;
    private EditText search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_drawer_content);

        initFont();
        userManager = UserManager.getInstance(getSharedPreferences("user_prefs", MODE_PRIVATE));

        container = findViewById(R.id.container);

        swipe_refresh = findViewById(R.id.swipe_refresh_layout);
        fromDate = findViewById(R.id.from_date);
        toDate = findViewById(R.id.to_date);
        search = findViewById(R.id.search);
        search_btn = findViewById(R.id.search_btn);
        builder = new AlertDialog.Builder(this);
        initDatePicker();
        initToolbar();
        initNavigationMenu();
        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", MODE_PRIVATE));
        service = RetrofitBuilder.createServiceWithAuth(Routes.class, tokenManager);
        dialog = KProgressHUD.create(MenuDrawerStudentsActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.loading))
                .setMaxProgress(100);
        dialog.show();
        nested_scroll_view = (NestedScrollView) findViewById(R.id.nested_scroll_view);
        // toggle input section
        bt_toggle_input = (ImageButton) findViewById(R.id.bt_toggle_input);
        bt_hide_input = (Button) findViewById(R.id.bt_hide_input);
        bt_save_input = (Button) findViewById(R.id.search_btn);
        lyt_expand_input = (View) findViewById(R.id.lyt_expand_input);
        lyt_expand_input.setVisibility(View.GONE);

        bt_toggle_input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSectionInput(bt_toggle_input);
            }
        });

        bt_hide_input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSectionInput(bt_toggle_input);
            }
        });

        bt_save_input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSectionInput(bt_toggle_input);
                initComponent();
            }
        });

        ((FloatingActionButton) findViewById(R.id.fab_add)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogFullscreen();
            }
        });

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        if (userManager.getUser().getAdmin().equals(1)) {
            nav_Menu.findItem(R.id.nav_my_courses).setVisible(false);
            nav_Menu.findItem(R.id.nav_enroll).setVisible(false);
        }

        final List<StudentModel> items = new ArrayList<>();
        Call<Entries<StudentModel>> call = service.getStudents(page, "", "", "");
        call.enqueue(new Callback<Entries<StudentModel>>() {
            @Override
            public void onResponse(Call<Entries<StudentModel>> call, Response<Entries<StudentModel>> response) {
                dialog.dismiss();
                items.addAll(response.body().getEntries());
                generateSectionList(items);
            }

            @Override
            public void onFailure(Call<Entries<StudentModel>> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullAndRefresh();
            }
        });
    }

    private void generateSectionList(List<StudentModel> instructors) {

        AdapterStudents adapter = new AdapterStudents(this, instructors);
        recyclerView = findViewById(R.id.card_courses_recycle_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(MenuDrawerStudentsActivity.this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new AdapterStudents.OnItemClickListener() {
            @Override
            public void onItemClick(View view, StudentModel obj, int position) {
                initComponent();
            }
        });

    }

    private void pullAndRefresh() {
        dialog.show();
        swipeProgress(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeProgress(false);
                initComponent();
            }
        }, 1000);
    }

    public void initComponent() {
        dialog.show();
        final List<StudentModel> items = new ArrayList<>();
        Call<Entries<StudentModel>> call = service.getStudents(page, fromDate.getText().toString(), toDate.getText().toString(), search.getText().toString());
        call.enqueue(new Callback<Entries<StudentModel>>() {
            @Override
            public void onResponse(Call<Entries<StudentModel>> call, Response<Entries<StudentModel>> response) {
                dialog.dismiss();
                swipeProgress(false);
                items.addAll(response.body().getEntries());
                generateSectionList(items);
            }

            @Override
            public void onFailure(Call<Entries<StudentModel>> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            return;
        }
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
        swipe_refresh.setColorSchemeColors(
                getResources().getColor(android.R.color.holo_blue_bright),
                getResources().getColor(android.R.color.holo_green_light),
                getResources().getColor(android.R.color.holo_orange_light),
                getResources().getColor(android.R.color.holo_red_light)
        );
    }

    private void showDialogFullscreen() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        StudentDialogFullscreenFragment newFragment = new StudentDialogFullscreenFragment();
        newFragment.setRequestCode(DIALOG_QUEST_CODE);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
        newFragment.setOnCallbackResult(new StudentDialogFullscreenFragment.CallbackResult() {
            @Override
            public void sendResult(int requestCode, Object obj) {
                if (requestCode == DIALOG_QUEST_CODE) {
                    initComponent();
                }
            }
        });
    }

    private void initFont() {
        FontsOverride.setDefaultFont(this, "SERIF", "fonts/JF-Flat-regular.ttf");
        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/JF-Flat-regular.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/JF-Flat-regular.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/JF-Flat-regular.ttf");
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Students");
        Tools.setSystemBarColor(this, R.color.colorPrimary);
    }

    private void initNavigationMenu() {
        final NavigationView nav_view = findViewById(R.id.nav_view);
        drawer = findViewById(R.id.drawer_layout);
        View headerView = nav_view.getHeaderView(0);
        String full_name = userManager.getUser().getFull_name();
        String email = userManager.getUser().getEmail();
        if (full_name != null) {
            drawer_user_name = headerView.findViewById(R.id.drawer_user_name);
            drawer_user_email = headerView.findViewById(R.id.drawer_user_email);
            drawer_user_name.setText(full_name);
            drawer_user_email.setText(email);
        }
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(final MenuItem item) {
                onItemNavigationClicked(item);
                return true;
            }
        });

        // open drawer at start
//        drawer.openDrawer(GravityCompat.START);
    }

    private void onItemNavigationClicked(MenuItem item) {
        if (item.getItemId() == R.id.nav_logout) {
            tokenManager.deleteToken();
            Intent i = new Intent(this.getApplicationContext(), LoginActivity.class);
            startActivity(i);
            finish();
        }
        switch (item.getItemId()) {
            case R.id.nav_courses:
                Intent i = new Intent(this.getApplicationContext(), MenuDrawerCoursesActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.nav_logout:
                tokenManager.deleteToken();
                Intent i_logout = new Intent(this.getApplicationContext(), LoginActivity.class);
                startActivity(i_logout);
                finish();
                break;
            case R.id.nav_sections:
                Intent i_nav_sections = new Intent(this.getApplicationContext(), MenuDrawerSectionsActivity.class);
                startActivity(i_nav_sections);
                finish();
                break;
            case R.id.nav_instructors:
                Intent i_nav_instructors = new Intent(this.getApplicationContext(), MenuDrawerInstructorsActivity.class);
                startActivity(i_nav_instructors);
                finish();
                break;
            case R.id.nav_students:
                Intent i_nav_students = new Intent(this.getApplicationContext(), MenuDrawerStudentsActivity.class);
                startActivity(i_nav_students);
                finish();
                break;
            case R.id.nav_enrollments:
                Intent i_nav_enrollments = new Intent(this.getApplicationContext(), MenuDrawerEnrollmentActivity.class);
                startActivity(i_nav_enrollments);
                finish();
                break;
        }
        drawer.closeDrawers();
    }

    private void toggleSectionInput(View view) {
        boolean show = toggleArrow(view);
        if (show) {
            ViewAnimation.expand(lyt_expand_input, new ViewAnimation.AnimListener() {
                @Override
                public void onFinish() {
//                    Tools.nestedScrollTo(nested_scroll_view, lyt_expand_input);
                }
            });
        } else {
            ViewAnimation.collapse(lyt_expand_input);
        }
    }

    public boolean toggleArrow(View view) {
        if (view.getRotation() == 0) {
            view.animate().setDuration(200).rotation(180);
            return true;
        } else {
            view.animate().setDuration(200).rotation(0);
            return false;
        }
    }

    private void initDatePicker() {
        findViewById(R.id.from_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogDatePickerFrom((TextView) view);
            }
        });
        findViewById(R.id.to_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogDatePickerTo((TextView) view);
            }
        });
    }

    private void dialogDatePickerFrom(final TextView bt) {
        Calendar cur_calender = Calendar.getInstance();
        DatePickerDialog datePicker = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        long date_ship_millis = calendar.getTimeInMillis();
                        ((TextView) findViewById(R.id.from_date)).setText(Tools.getFormattedDateSimple(date_ship_millis));

                    }
                },
                cur_calender.get(Calendar.YEAR),
                cur_calender.get(Calendar.MONTH),
                cur_calender.get(Calendar.DAY_OF_MONTH)
        );
        //set dark theme
        datePicker.setThemeDark(true);
        datePicker.setAccentColor(getResources().getColor(R.color.colorPrimary));
        datePicker.setMaxDate(cur_calender);
        datePicker.show(getFragmentManager(), "Datepickerdialog");
    }

    private void dialogDatePickerTo(final TextView bt) {
        Calendar cur_calender = Calendar.getInstance();
        DatePickerDialog datePicker = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        long date_ship_millis = calendar.getTimeInMillis();
                        ((TextView) findViewById(R.id.to_date)).setText(Tools.getFormattedDateSimple(date_ship_millis));

                    }
                },
                cur_calender.get(Calendar.YEAR),
                cur_calender.get(Calendar.MONTH),
                cur_calender.get(Calendar.DAY_OF_MONTH)
        );
        //set dark theme
        datePicker.setThemeDark(true);
        datePicker.setAccentColor(getResources().getColor(R.color.colorPrimary));
        datePicker.setMaxDate(cur_calender);
        datePicker.show(getFragmentManager(), "Datepickerdialog");
    }

}