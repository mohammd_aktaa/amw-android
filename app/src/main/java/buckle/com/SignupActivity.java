package buckle.com;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.List;
import java.util.Map;

import buckle.com.model.Auth;
import buckle.com.model.MessageModel;
import buckle.com.model.Utils;
import buckle.com.network.RetrofitBuilder;
import buckle.com.network.Routes;
import buckle.com.network.TokenManager;
import buckle.com.network.UserManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    Routes service;
    TokenManager tokenManager;
    UserManager userManager;
    EditText password, email, last_name, first_name, username, reg_year, address, mobile_number, name;
    RadioGroup gender;
    RadioButton radioButton;
    RadioGroup type;
    RadioButton radioButtonType;
    RadioButton typeR;
    private ProgressBar progress_bar;
    private FloatingActionButton fab;
    private View parent_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", MODE_PRIVATE));
        userManager = UserManager.getInstance(getSharedPreferences("user_prefs", MODE_PRIVATE));
        service = RetrofitBuilder.createServiceWithAuth(Routes.class, tokenManager);

        if (tokenManager.getToken().getAccess_token() != null) {
            startActivity(new Intent(SignupActivity.this, MainActivity.class));
            finish();
        }

        parent_view = findViewById(android.R.id.content);
        progress_bar = findViewById(R.id.progress_bar);
        fab = findViewById(R.id.fab);

        findViewById(R.id.already_have_account).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                signUp();
            }
        });
        name = findViewById(R.id.name);
        password = findViewById(R.id.password);
        email = findViewById(R.id.email);
        last_name = findViewById(R.id.last_name);
        first_name = findViewById(R.id.first_name);
        username = findViewById(R.id.username);
        reg_year = findViewById(R.id.reg_year);
        address = findViewById(R.id.address);
        mobile_number = findViewById(R.id.mobile_number);
        gender = findViewById(R.id.gender);
        type = findViewById(R.id.type);
        name.setVisibility(View.GONE);
        type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int ID = group.getCheckedRadioButtonId();
                typeR = findViewById(ID);
                String typeStr = typeR.getText().toString();
                if (typeStr.equals("Student")) {
                    name.setVisibility(View.GONE);
                    last_name.setVisibility(View.VISIBLE);
                    first_name.setVisibility(View.VISIBLE);
                    reg_year.setVisibility(View.VISIBLE);
                    address.setVisibility(View.VISIBLE);
                    mobile_number.setVisibility(View.VISIBLE);
                } else {
                    name.setVisibility(View.VISIBLE);
                    last_name.setVisibility(View.GONE);
                    first_name.setVisibility(View.GONE);
                    reg_year.setVisibility(View.GONE);
                    address.setVisibility(View.GONE);
                    mobile_number.setVisibility(View.GONE);
                }
            }
        });
        first_name.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        last_name.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
    }

    private void signUp() {
        int id = gender.getCheckedRadioButtonId();
        radioButton = findViewById(id);
        int idType = type.getCheckedRadioButtonId();
        radioButtonType = findViewById(idType);
        progress_bar.setVisibility(View.VISIBLE);
        fab.setAlpha(0f);
        Call<Auth> call = service.registerAdmin(username.getText().toString(), name.getText().toString(), email.getText().toString(), password.getText().toString(), radioButtonType.getText().toString().toLowerCase().equalsIgnoreCase("Student") ? "student" : "api");
        if (radioButtonType.getText().toString().toLowerCase().equals("student"))
            call = service.register(password.getText().toString(), email.getText().toString(), last_name.getText().toString(), first_name.getText().toString(), username.getText().toString(), reg_year.getText().toString(), address.getText().toString(), mobile_number.getText().toString(), radioButton.getText().toString().toLowerCase(), radioButtonType.getText().toString().toLowerCase().equalsIgnoreCase("Student") ? "student" : "api");

        call.enqueue(new Callback<Auth>() {
            @Override
            public void onResponse(Call<Auth> call, Response<Auth> response) {
                if (response.isSuccessful()) {
                    tokenManager.saveToken(response.body());
                    userManager.saveUser(response.body().getData().getUser());
                    startActivity(new Intent(SignupActivity.this, MainActivity.class));
                    finish();
                } else {
                    if (response.code() == 400) {
                        handleInlineErrors(response.errorBody());
                        progress_bar.setVisibility(View.GONE);
                        fab.setAlpha(1f);
                    }
                    if (response.code() == 401) {
                        handleInlineErrors(response.errorBody());
                        progress_bar.setVisibility(View.GONE);
                        fab.setAlpha(1f);
                    }
                    if (response.code() == 422) {
                        handleErrors(response.errorBody());
                        progress_bar.setVisibility(View.GONE);
                        fab.setAlpha(1f);
                    }
                }
            }

            @Override
            public void onFailure(Call<Auth> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void handleInlineErrors(ResponseBody response) {

        MessageModel apiError = Utils.converErrors(response);

        email.setError(apiError.getMessage());
        password.setError(apiError.getMessage());
    }

    private void handleErrors(ResponseBody response) {
        MessageModel apiError = Utils.converErrors(response);
        for (Map.Entry<String, List<String>> error : apiError.getErrors().entrySet()) {
            if (error.getKey().equals("email")) {
                String e = error.getValue().get(0);
                email.setError(e);
            }
            if (error.getKey().equals("password")) {
                String e = error.getValue().get(0);
                password.setError(e);
            }
            if (error.getKey().equals("birth_date")) {
                username.setError(error.getValue().get(0));
            }
            if (error.getKey().equals("reg_year")) {
                reg_year.setError(error.getValue().get(0));
            }
            if (error.getKey().equals("address")) {
                address.setError(error.getValue().get(0));
            }
            if (error.getKey().equals("mobile_number")) {
                mobile_number.setError(error.getValue().get(0));
            }
            if (error.getKey().equals("first_name")) {
                first_name.setError(error.getValue().get(0));
            }
            if (error.getKey().equals("last_name")) {
                last_name.setError(error.getValue().get(0));
            }
            if (error.getKey().equals("name")) {
                name.setError(error.getValue().get(0));
            }
        }
    }
}
