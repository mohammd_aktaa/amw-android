package buckle.com.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import buckle.com.MenuDrawerInstructorsActivity;
import buckle.com.R;
import buckle.com.model.CourseModel;
import buckle.com.model.MessageModel;
import buckle.com.model.SectionModel;
import buckle.com.model.Utils;
import buckle.com.network.RetrofitBuilder;
import buckle.com.network.Routes;
import buckle.com.network.TokenManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class SectionDialogFullscreenFragment extends DialogFragment {

    public CallbackResult callbackResult;
    Routes service;
    TokenManager tokenManager;
    SectionModel section;
    String type = "ADD";
    RadioGroup gender;
    RadioButton genderValue;
    //    private AppCompatCheckBox cb_allday;
//    private AppCompatSpinner spn_timezone;
    String[] arr = {"Paries,France", "PA,United States", "Parana,Brazil", "Padua,Italy", "Pasadena,CA,United States"};
    List<CourseModel> courses = new ArrayList<CourseModel>();
    AutoCompleteTextView autocomplete;
    private int request_code = 0;
    private View root_view;
    private EditText section_no, room_no, time, instructor_id, course_id;
    private TextView dialog_title;
    private int mHour, mMinute;

    public void setOnCallbackResult(final CallbackResult callbackResult) {
        this.callbackResult = callbackResult;
    }

    public void setValues(SectionModel section, String type) {
        this.section = new SectionModel();
        this.section.setId(section.getId());
        this.section.setCourse_id(section.getCourse_id());
        this.section.setSection_no(section.getSection_no());
        this.section.setInstructor_id(section.getInstructor_id());
        this.section.setRoom_no(section.getRoom_no());
        this.section.setTime(section.getTime());
        this.type = type;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        root_view = inflater.inflate(R.layout.dialog_section, container, false);
        tokenManager = TokenManager.getInstance(this.getActivity().getSharedPreferences("prefs", MODE_PRIVATE));
        service = RetrofitBuilder.createServiceWithAuth(Routes.class, tokenManager);
        dialog_title = (TextView) root_view.findViewById(R.id.dialog_title);
        section_no = (EditText) root_view.findViewById(R.id.section_no);
        room_no = (EditText) root_view.findViewById(R.id.room_no);
        time = (EditText) root_view.findViewById(R.id.time);
        instructor_id = (EditText) root_view.findViewById(R.id.instructor_id);
        course_id = (EditText) root_view.findViewById(R.id.course_id);
        initTimePicker();
//        gender.setText(this.section.getGender());

        if (type.equals("UPDATE")) {
            dialog_title.setText("Update section");
            section_no.setText(this.section.getSection_no().toString());
            room_no.setText(this.section.getRoom_no().toString());
            time.setText(this.section.getTime());
            instructor_id.setText(this.section.getInstructor_id().toString());
            course_id.setText(this.section.getCourse_id().toString());
        }
        courses.add(new CourseModel());
        autocomplete = (AutoCompleteTextView) root_view.findViewById(R.id.autoCompleteTextView1);

        ArrayAdapter<CourseModel> adapter = new ArrayAdapter<CourseModel>(this.getContext(), android.R.layout.select_dialog_item, courses);

        autocomplete.setThreshold(2);
        autocomplete.setAdapter(adapter);
        autocomplete.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ((ImageButton) root_view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        ((Button) root_view.findViewById(R.id.bt_save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendDataResult();

            }
        });

        return root_view;
    }

    private void initTimePicker() {
        root_view.findViewById(R.id.time).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTimePicker((TextView) view);
            }
        });
    }

    private void dialogTimePicker(final TextView bt) {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        if (time.getText().toString().equals("")) {
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

        } else {
            mHour = Integer.parseInt(time.getText().toString().split(":")[0]);
            mMinute = Integer.parseInt(time.getText().toString().split(":")[1]);
        }
        ;

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this.getContext(),
                new TimePickerDialog.OnTimeSetListener() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        ((TextView) root_view.findViewById(R.id.time)).setText(hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, true);
        timePickerDialog.show();
    }

    private void sendDataResult() {
        int id = gender.getCheckedRadioButtonId();
        genderValue = root_view.findViewById(id);
        if (type.equals("ADD")) {
            Call<MessageModel> call = service.addSection(Integer.parseInt(section_no.getText().toString()), Integer.parseInt(room_no.getText().toString()), time.getText().toString().toLowerCase(), Integer.parseInt(instructor_id.getText().toString()), Integer.parseInt(course_id.getText().toString()));
            call.enqueue(new Callback<MessageModel>() {
                @Override
                public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {
                    if (response.isSuccessful()) {
                        if (callbackResult != null) {
                            callbackResult.sendResult(request_code, null);
                            dismiss();
                        }
                    } else {
                        if (response.code() == 400) {
                            handleInlineErrors(response.errorBody());
                        }
                        if (response.code() == 401) {
                            handleInlineErrors(response.errorBody());
                        }
                        if (response.code() == 422) {
                            handleErrors(response.errorBody());
                        }
                    }

                }

                @Override
                public void onFailure(Call<MessageModel> call, Throwable t) {
                    Toast.makeText(getActivity().getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                }
            });
        } else {
            Call<MessageModel> call = service.updateSection(section.getId(), Integer.parseInt(section_no.getText().toString()), Integer.parseInt(room_no.getText().toString()), time.getText().toString().toLowerCase(), Integer.parseInt(instructor_id.getText().toString()), Integer.parseInt(course_id.getText().toString()));
            call.enqueue(new Callback<MessageModel>() {
                @Override
                public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {
                    if (response.isSuccessful()) {
                        if (callbackResult != null) {
                            callbackResult.sendResult(request_code, null);
                            dismiss();
                        }
                        Intent i = new Intent(getActivity().getApplicationContext(), MenuDrawerInstructorsActivity.class);
                        startActivity(i);
                    } else {
                        if (response.code() == 400) {
                            handleInlineErrors(response.errorBody());
                        }
                        if (response.code() == 401) {
                            handleInlineErrors(response.errorBody());
                        }
                        if (response.code() == 422) {
                            handleErrors(response.errorBody());
                        }
                    }

                }

                @Override
                public void onFailure(Call<MessageModel> call, Throwable t) {
                    Toast.makeText(getActivity().getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                }
            });
        }
    }

    private void handleInlineErrors(ResponseBody response) {

        MessageModel apiError = Utils.converErrors(response);

        section_no.setError(apiError.getMessage());
        room_no.setError(apiError.getMessage());
        time.setError(apiError.getMessage());
        instructor_id.setError(apiError.getMessage());
        course_id.setError(apiError.getMessage());
    }

    private void handleErrors(ResponseBody response) {

        MessageModel apiError = Utils.converErrors(response);
        for (Map.Entry<String, List<String>> error : apiError.getErrors().entrySet()) {
            if (error.getKey().equals("section_no")) {
                String e = error.getValue().get(0);
                section_no.setError(e);
            }
            if (error.getKey().equals("room_no")) {
                String e = error.getValue().get(0);
                room_no.setError(e);
            }
            if (error.getKey().equals("time")) {
                String e = error.getValue().get(0);
                time.setError(e);
            }
            if (error.getKey().equals("instructor_id")) {
                String e = error.getValue().get(0);
                instructor_id.setError(e);
            }
            if (error.getKey().equals("course_id")) {
                String e = error.getValue().get(0);
                course_id.setError(e);
            }
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public void setRequestCode(int request_code) {
        this.request_code = request_code;
    }

    public interface CallbackResult {
        void sendResult(int requestCode, Object obj);
    }

}