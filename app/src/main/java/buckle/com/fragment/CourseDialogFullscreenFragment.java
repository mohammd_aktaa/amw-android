package buckle.com.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Map;

import buckle.com.MenuDrawerCoursesActivity;
import buckle.com.R;
import buckle.com.model.CourseModel;
import buckle.com.model.MessageModel;
import buckle.com.model.Utils;
import buckle.com.network.RetrofitBuilder;
import buckle.com.network.Routes;
import buckle.com.network.TokenManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class CourseDialogFullscreenFragment extends DialogFragment {

    public CallbackResult callbackResult;
    Routes service;
    TokenManager tokenManager;
    CourseModel course;
    String type = "ADD";
    private int request_code = 0;
    private View root_view;
    private EditText title, hours;
    private TextView dialog_title;

    public void setOnCallbackResult(final CallbackResult callbackResult) {
        this.callbackResult = callbackResult;
    }

    public void setValues(int id, String title, int hours, String type) {
        this.course = new CourseModel();
        this.course.setId(id);
        this.course.setTitle(title);
        this.course.setHours(hours);
        this.type = type;
    }
//    private AppCompatCheckBox cb_allday;
//    private AppCompatSpinner spn_timezone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        root_view = inflater.inflate(R.layout.dialog_course, container, false);
        tokenManager = TokenManager.getInstance(this.getActivity().getSharedPreferences("prefs", MODE_PRIVATE));
        service = RetrofitBuilder.createServiceWithAuth(Routes.class, tokenManager);
        dialog_title = (TextView) root_view.findViewById(R.id.dialog_title);
        title = (EditText) root_view.findViewById(R.id.title);
        hours = (EditText) root_view.findViewById(R.id.hours);

        if (type.equals("UPDATE")) {
            dialog_title.setText("Update Course");
            title.setText(this.course.getTitle());
            hours.setText(this.course.getHours().toString());
        }
        ((ImageButton) root_view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        ((Button) root_view.findViewById(R.id.bt_save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendDataResult();
                dismiss();
            }
        });

        return root_view;
    }

    private void sendDataResult() {
        if (type.equals("ADD")) {
            Call<MessageModel> call = service.addCourse(title.getText().toString(), Integer.parseInt(hours.getText().toString()));
            call.enqueue(new Callback<MessageModel>() {
                @Override
                public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {
                    if (response.isSuccessful()) {
                        if (callbackResult != null) {
                            callbackResult.sendResult(request_code, null);
                        }
                    } else {
                        if (response.code() == 400) {
                            handleInlineErrors(response.errorBody());
                        }
                        if (response.code() == 401) {
                            handleInlineErrors(response.errorBody());
                        }
                        if (response.code() == 422) {
                            handleErrors(response.errorBody());
                        }
                    }

                }

                @Override
                public void onFailure(Call<MessageModel> call, Throwable t) {
                    Toast.makeText(getActivity().getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                }
            });
        } else {
            Call<MessageModel> call = service.updateCourse(course.getId(), title.getText().toString(), Integer.parseInt(hours.getText().toString()));
            call.enqueue(new Callback<MessageModel>() {
                @Override
                public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {
                    if (response.isSuccessful()) {
                        if (callbackResult != null) {
                            callbackResult.sendResult(request_code, null);
                        }
                        Intent i = new Intent(getActivity().getApplicationContext(), MenuDrawerCoursesActivity.class);
                        startActivity(i);
                    } else {
                        if (response.code() == 400) {
                            handleInlineErrors(response.errorBody());
                        }
                        if (response.code() == 401) {
                            handleInlineErrors(response.errorBody());
                        }
                        if (response.code() == 422) {
                            handleErrors(response.errorBody());
                        }
                    }

                }

                @Override
                public void onFailure(Call<MessageModel> call, Throwable t) {
                    Toast.makeText(getActivity().getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                }
            });
        }
    }

    private void handleInlineErrors(ResponseBody response) {

        MessageModel apiError = Utils.converErrors(response);

        title.setError(apiError.getMessage());
        hours.setError(apiError.getMessage());
    }

    private void handleErrors(ResponseBody response) {

        MessageModel apiError = Utils.converErrors(response);
        for (Map.Entry<String, List<String>> error : apiError.getErrors().entrySet()) {
            if (error.getKey().equals("title")) {
                String e = error.getValue().get(0);
                title.setError(e);
            }
            if (error.getKey().equals("hours")) {
                String e = error.getValue().get(0);
                hours.setError(e);
            }
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public void setRequestCode(int request_code) {
        this.request_code = request_code;
    }

    public interface CallbackResult {
        void sendResult(int requestCode, Object obj);
    }

}