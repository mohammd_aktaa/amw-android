package buckle.com.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Map;

import buckle.com.MenuDrawerInstructorsActivity;
import buckle.com.R;
import buckle.com.model.InstructorModel;
import buckle.com.model.MessageModel;
import buckle.com.model.Utils;
import buckle.com.network.RetrofitBuilder;
import buckle.com.network.Routes;
import buckle.com.network.TokenManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class InstructorDialogFullscreenFragment extends DialogFragment {

    public CallbackResult callbackResult;
    Routes service;
    TokenManager tokenManager;
    InstructorModel instructor;
    String type = "ADD";
    RadioGroup gender;
    RadioButton genderValue;
    private int request_code = 0;
    private View root_view;
    private EditText address, mobile_number, first_name, last_name;
    private TextView dialog_title;

    public void setOnCallbackResult(final CallbackResult callbackResult) {
        this.callbackResult = callbackResult;
    }

    public void setValues(InstructorModel instructor, String type) {
        this.instructor = new InstructorModel();
        this.instructor.setId(instructor.getId());
        this.instructor.setFirst_name(instructor.getFirst_name());
        this.instructor.setLast_name(instructor.getLast_name());
        this.instructor.setGender(instructor.getGender());
        this.instructor.setAddress(instructor.getAddress());
        this.instructor.setMobile_number(instructor.getMobile_number());
        this.type = type;
    }
//    private AppCompatCheckBox cb_allday;
//    private AppCompatSpinner spn_timezone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        root_view = inflater.inflate(R.layout.dialog_instructor, container, false);
        tokenManager = TokenManager.getInstance(this.getActivity().getSharedPreferences("prefs", MODE_PRIVATE));
        service = RetrofitBuilder.createServiceWithAuth(Routes.class, tokenManager);
        dialog_title = (TextView) root_view.findViewById(R.id.dialog_title);
        gender = root_view.findViewById(R.id.gender);
        address = (EditText) root_view.findViewById(R.id.address);
        mobile_number = (EditText) root_view.findViewById(R.id.mobile_number);
        first_name = (EditText) root_view.findViewById(R.id.first_name);
        last_name = (EditText) root_view.findViewById(R.id.last_name);

//        gender.setText(this.instructor.getGender());

        if (type.equals("UPDATE")) {
            dialog_title.setText("Update instructor");
            address.setText(this.instructor.getAddress());
            mobile_number.setText(this.instructor.getMobile_number());
            first_name.setText(this.instructor.getFirst_name());
            last_name.setText(this.instructor.getLast_name());
            if (instructor.getGender().toLowerCase().equals("female")) {
                gender.check(R.id.radio_female);
            }else{
                gender.check(R.id.radio_male);
            }
        }
        ((ImageButton) root_view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        ((Button) root_view.findViewById(R.id.bt_save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendDataResult();

            }
        });

        return root_view;
    }

    private void sendDataResult() {
        int id = gender.getCheckedRadioButtonId();
        genderValue = root_view.findViewById(id);
        if (type.equals("ADD")) {
            Call<MessageModel> call = service.addInstructor(first_name.getText().toString(), last_name.getText().toString(), genderValue.getText().toString().toLowerCase(), address.getText().toString(), mobile_number.getText().toString());
            call.enqueue(new Callback<MessageModel>() {
                @Override
                public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {
                    if (response.isSuccessful()) {
                        if (callbackResult != null) {
                            callbackResult.sendResult(request_code, null);
                            dismiss();
                        }
                    } else {
                        if (response.code() == 400) {
                            handleInlineErrors(response.errorBody());
                        }
                        if (response.code() == 401) {
                            handleInlineErrors(response.errorBody());
                        }
                        if (response.code() == 422) {
                            handleErrors(response.errorBody());
                        }
                    }

                }

                @Override
                public void onFailure(Call<MessageModel> call, Throwable t) {
                    Toast.makeText(getActivity().getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                }
            });
        } else {
            Call<MessageModel> call = service.updateInstructor(instructor.getId(), first_name.getText().toString(), last_name.getText().toString(), genderValue.getText().toString().toLowerCase(), address.getText().toString(), mobile_number.getText().toString());
            call.enqueue(new Callback<MessageModel>() {
                @Override
                public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {
                    if (response.isSuccessful()) {
                        if (callbackResult != null) {
                            callbackResult.sendResult(request_code, null);
                            dismiss();
                        }
                        Intent i = new Intent(getActivity().getApplicationContext(), MenuDrawerInstructorsActivity.class);
                        startActivity(i);
                    } else {
                        if (response.code() == 400) {
                            handleInlineErrors(response.errorBody());
                        }
                        if (response.code() == 401) {
                            handleInlineErrors(response.errorBody());
                        }
                        if (response.code() == 422) {
                            handleErrors(response.errorBody());
                        }
                    }

                }

                @Override
                public void onFailure(Call<MessageModel> call, Throwable t) {
                    Toast.makeText(getActivity().getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                }
            });
        }
    }

    private void handleInlineErrors(ResponseBody response) {

        MessageModel apiError = Utils.converErrors(response);

        first_name.setError(apiError.getMessage());
        last_name.setError(apiError.getMessage());
        address.setError(apiError.getMessage());
        mobile_number.setError(apiError.getMessage());
    }

    private void handleErrors(ResponseBody response) {

        MessageModel apiError = Utils.converErrors(response);
        for (Map.Entry<String, List<String>> error : apiError.getErrors().entrySet()) {
            if (error.getKey().equals("first_name")) {
                String e = error.getValue().get(0);
                first_name.setError(e);
            }
            if (error.getKey().equals("last_name")) {
                String e = error.getValue().get(0);
                last_name.setError(e);
            }
            if (error.getKey().equals("address")) {
                String e = error.getValue().get(0);
                address.setError(e);
            }
            if (error.getKey().equals("mobile_number")) {
                String e = error.getValue().get(0);
                mobile_number.setError(e);
            }
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public void setRequestCode(int request_code) {
        this.request_code = request_code;
    }

    public interface CallbackResult {
        void sendResult(int requestCode, Object obj);
    }

}