package buckle.com;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


import java.util.List;
import java.util.Map;

import buckle.com.model.Auth;
import buckle.com.model.MessageModel;
import buckle.com.model.Utils;
import buckle.com.network.RetrofitBuilder;
import buckle.com.network.Routes;
import buckle.com.network.TokenManager;
import buckle.com.network.UserManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    Routes service;
    TokenManager tokenManager;
    UserManager userManager;
    EditText email, password;
    Button loginBtn;
    RadioGroup type;
    RadioButton radioButton;
    private ProgressBar progress_bar;
    private FloatingActionButton fab;
    private View parent_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", MODE_PRIVATE));
        userManager = UserManager.getInstance(getSharedPreferences("user_prefs", MODE_PRIVATE));
        service = RetrofitBuilder.createServiceWithAuth(Routes.class, tokenManager);

        if (tokenManager.getToken().getAccess_token() != null) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
            return;
        }

        parent_view = findViewById(android.R.id.content);
        progress_bar = findViewById(R.id.progress_bar);
        fab = findViewById(R.id.fab);

        findViewById(R.id.sign_up_for_account).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), SignupActivity.class));
                finish();

            }
        });
        type = findViewById(R.id.type);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                login();
            }
        });
    }

    private void login() {
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        Integer id = type.getCheckedRadioButtonId();
        radioButton = findViewById(id);
        final String typeString = radioButton.getText().toString().toLowerCase();
        if (email.getText().toString().equals("") || password.getText().toString().equals("")) {
            Snackbar.make(parent_view, "Please fill out all inputs.", Snackbar.LENGTH_LONG).show();
        } else {

            progress_bar.setVisibility(View.VISIBLE);
            fab.setAlpha(0f);

            Call<Auth> call = service.login(email.getText().toString(), password.getText().toString(), typeString.equalsIgnoreCase("Student") ? "student" : "api");
            call.enqueue(new Callback<Auth>() {
                @Override
                public void onResponse(Call<Auth> call, Response<Auth> response) {
                    if (response.isSuccessful()) {
                        tokenManager.saveToken(response.body());
                        userManager.saveUser(response.body().getData().getUser());
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    } else {
                        if (response.code() == 400) {
                            handleErrors(response.errorBody());
                            progress_bar.setVisibility(View.GONE);
                            fab.setAlpha(1f);
                        }
                        if (response.code() == 401) {
                            handleErrors(response.errorBody());
                            progress_bar.setVisibility(View.GONE);
                            fab.setAlpha(1f);
                        }
                        if (response.code() == 422) {
                            handleErrors(response.errorBody());
                            progress_bar.setVisibility(View.GONE);
                            fab.setAlpha(1f);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Auth> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void handleErrors(ResponseBody response) {

        MessageModel apiError = Utils.converErrors(response);
        for (Map.Entry<String, List<String>> error : apiError.getErrors().entrySet()) {
            if (error.getKey().equals("email")) {
                String e = error.getValue().get(0);
                email.setError(e);
            }
            if (error.getKey().equals("password")) {
                String e = error.getValue().get(0);
                password.setError(e);
            }
        }
    }

}
