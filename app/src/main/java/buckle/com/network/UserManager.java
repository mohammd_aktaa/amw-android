package buckle.com.network;

import android.content.SharedPreferences;

import buckle.com.model.UserModel;


public class UserManager {

    private static UserManager INSTANCE = null;
    private SharedPreferences user_prefs;
    private SharedPreferences.Editor editor;

    private UserManager(SharedPreferences user_prefs) {
        this.user_prefs = user_prefs;
        this.editor = user_prefs.edit();
    }

    public static synchronized UserManager getInstance(SharedPreferences user_prefs) {
        if (INSTANCE == null) {
            INSTANCE = new UserManager(user_prefs);
        }
        return INSTANCE;
    }

    public void saveUser(UserModel user) {
        editor.putString("FULL_NAME", user.getFull_name()).commit();
        editor.putString("EMAIL", user.getEmail()).commit();
        editor.putInt("IS_ADMIN", user.getAdmin() ==  null ? 0 : 1).commit();
    }

    public void deleteUser() {
        editor.remove("FULL_NAME").commit();
        editor.remove("EMAIL").commit();
        editor.remove("AVATAR").commit();
    }

    public UserModel getUser() {
        UserModel user = new UserModel();
        user.setFull_name(user_prefs.getString("FULL_NAME", null));
        user.setEmail(user_prefs.getString("EMAIL", null));
        user.setAdmin(user_prefs.getInt("IS_ADMIN", 1));
        return user;
    }


}