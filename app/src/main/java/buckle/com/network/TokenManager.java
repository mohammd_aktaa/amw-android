package buckle.com.network;

import android.content.SharedPreferences;

import buckle.com.model.AccessToken;
import buckle.com.model.Auth;


public class TokenManager {

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    private static TokenManager INSTANCE = null;

    private TokenManager(SharedPreferences prefs){
        this.prefs = prefs;
        this.editor = prefs.edit();
    }

    public static synchronized TokenManager getInstance(SharedPreferences prefs){
        if(INSTANCE == null){
            INSTANCE = new TokenManager(prefs);
        }
        return INSTANCE;
    }

    public void saveToken(Auth authObj){
        editor.putString("ACCESS_TOKEN", authObj.getData().getAccess_token()).commit();
        editor.putString("ACCESS_TOKEN", authObj.getData().getAccess_token()).commit();
    }

    public void deleteToken(){
        editor.remove("ACCESS_TOKEN").commit();
        editor.remove("REFRESH_TOKEN").commit();
    }

    public AccessToken getToken(){
        AccessToken token = new AccessToken();
        token.setAccess_token(prefs.getString("ACCESS_TOKEN", null));
        return token;
    }



}