package buckle.com.network;


import java.util.List;

import buckle.com.model.Auth;
import buckle.com.model.CourseModel;
import buckle.com.model.EnrollmentModel;
import buckle.com.model.Entries;
import buckle.com.model.InstructorModel;
import buckle.com.model.MessageModel;
import buckle.com.model.SectionModel;
import buckle.com.model.StudentModel;
import buckle.com.model.UserModel;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by praka on 12/24/2017.
 */

public interface Routes {

    @GET("api/students-enrollments")
    Call<Entries<EnrollmentModel>> getEnrollments(@Query("page") int page, @Query("fromDate") String fromDate, @Query("toDate") String toDate, @Query("query") String query);

    @GET("api/students/my-courses/list")
    Call<Entries<EnrollmentModel>> getMyCourses(@Query("page") int page, @Query("fromDate") String fromDate, @Query("toDate") String toDate, @Query("query") String query);

    @GET("api/student-courses")
    Call<Entries<CourseModel>> getStudentCourses(@Query("page") int page, @Query("per_page") int per_page, @Query("fromDate") String fromDate, @Query("toDate") String toDate, @Query("query") String query);

    @GET("api/student-courses/{course}/sections")
    Call<List<SectionModel>> getCourseSections(@Path("course") int course);

    @POST("api/student-courses/enroll")
    @FormUrlEncoded
    Call<MessageModel> enroll(
            @Field("section_id") int section_id,
            @Field("section_no") int section_no,
            @Field("instructor_id") int instructor_id,
            @Field("course_id") int course_id);

    @GET("api/courses")
    Call<Entries<CourseModel>> getCourses(@Query("page") int page, @Query("per_page") int per_page, @Query("fromDate") String fromDate, @Query("toDate") String toDate, @Query("query") String query);

    @DELETE("api/courses/{course}")
    Call<MessageModel> deleteCourse(@Path("course") int course);

    @POST("api/courses")
    @FormUrlEncoded
    Call<MessageModel> addCourse(@Field("title") String title, @Field("hours") int hours);

    @PATCH("api/courses/{course}")
    @FormUrlEncoded
    Call<MessageModel> updateCourse(@Path("course") int course, @Field("title") String title, @Field("hours") int hours);

    @GET("api/sections")
    Call<Entries<SectionModel>> getSections(@Query("page") int page, @Query("fromDate") String fromDate, @Query("toDate") String toDate, @Query("query") String query);

    @DELETE("api/sections/{section}")
    Call<MessageModel> deleteSection(@Path("section") int section);

    @POST("api/sections")
    @FormUrlEncoded
    Call<MessageModel> addSection(
            @Field("section_no") int section_no,
            @Field("room_no") int room_no,
            @Field("time") String time,
            @Field("instructor_id") int instructor_id,
            @Field("course_id") int course_id);

    @PATCH("api/sections/{section}")
    @FormUrlEncoded
    Call<MessageModel> updateSection(@Path("section") int section, @Field("section_no") int section_no,
                                     @Field("room_no") int room_no,
                                     @Field("time") String time,
                                     @Field("instructor_id") int instructor_id,
                                     @Field("course_id") int course_id);

    @GET("api/instructors")
    Call<Entries<InstructorModel>> getInstructors(@Query("page") int page, @Query("fromDate") String fromDate, @Query("toDate") String toDate, @Query("query") String query);

    @DELETE("api/instructors/{instructor}")
    Call<MessageModel> deleteInstructor(@Path("instructor") int instructor);

    @POST("api/instructors")
    @FormUrlEncoded
    Call<MessageModel> addInstructor(
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("gender") String gender,
            @Field("address") String address,
            @Field("mobile_number") String mobile_number
    );

    @PATCH("api/instructors/{instructor}")
    @FormUrlEncoded
    Call<MessageModel> updateInstructor(@Path("instructor") int instructor, @Field("first_name") String first_name,
                                        @Field("last_name") String last_name,
                                        @Field("gender") String gender,
                                        @Field("address") String address,
                                        @Field("mobile_number") String mobile_number);

    @GET("api/students")
    Call<Entries<StudentModel>> getStudents(@Query("page") int page, @Query("fromDate") String fromDate, @Query("toDate") String toDate, @Query("query") String query);

    @DELETE("api/students/{student}")
    Call<MessageModel> deleteStudent(@Path("student") int Student);

    @POST("api/students")
    @FormUrlEncoded
    Call<MessageModel> addStudent(
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("gender") String gender,
            @Field("address") String address,
            @Field("email") String email,
            @Field("username") String username,
            @Field("reg_year") String reg_year,
            @Field("password") String password,
            @Field("mobile_number") String mobile_number
    );

    @PATCH("api/students/{student}")
    @FormUrlEncoded
    Call<MessageModel> updateStudent(@Path("student") int Student, @Field("first_name") String first_name,
                                     @Field("last_name") String last_name,
                                     @Field("gender") String gender,
                                     @Field("address") String address,
                                     @Field("email") String email,
                                     @Field("username") String username,
                                     @Field("reg_year") String reg_year,
                                     @Field("mobile_number") String mobile_number);


    @GET("api/user")
    Call<UserModel> getAuthUser();

    @POST("api/register")
    @FormUrlEncoded
    Call<Auth> register(
            @Field("password") String password,
            @Field("email") String email,
            @Field("last_name") String last_name,
            @Field("first_name") String first_name,
            @Field("username") String username,
            @Field("reg_year") String reg_year,
            @Field("address") String address,
            @Field("mobile_number") String mobile_number,
            @Field("gender") String gender,
            @Field("type") String type);

    @POST("api/register")
    @FormUrlEncoded
    Call<Auth> registerAdmin(
            @Field("username") String username,
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password,
            @Field("type") String type);

    @POST("api/login")
    @FormUrlEncoded
    Call<Auth> login(@Field("email") String email, @Field("password") String password, @Field("type") String type);
}
