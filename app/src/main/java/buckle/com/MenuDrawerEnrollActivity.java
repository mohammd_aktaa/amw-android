package buckle.com;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.ArrayList;
import java.util.List;

import buckle.com.adapters.AdapterCourses;
import buckle.com.adapters.AdapterEnrollCourses;
import buckle.com.adapters.AdapterEnrollSections;
import buckle.com.model.CourseModel;
import buckle.com.model.Entries;
import buckle.com.model.MessageModel;
import buckle.com.model.SectionModel;
import buckle.com.network.RetrofitBuilder;
import buckle.com.network.Routes;
import buckle.com.network.TokenManager;
import buckle.com.network.UserManager;
import buckle.com.utils.Tools;
import buckle.com.utils.ViewAnimation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuDrawerEnrollActivity extends AppCompatActivity {
    public static final int DIALOG_QUEST_CODE = 300;
    private static final int MAX_STEP = 2;
    Routes service;
    TokenManager tokenManager;
    int page = 0;
    RelativeLayout container;
    UserManager userManager;
    AlertDialog.Builder builder;
    KProgressHUD dialog;
    SwipeRefreshLayout swipe_refresh;
    NavigationView navigationView;
    int course_id = 0;
    int section_id = 0;
    int section_no;
    int instructor_id;
    private ActionBar actionBar;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private boolean is_account_mode = false;
    private TextView drawer_user_name, drawer_user_email;
    private RecyclerView recyclerView, card_sections_recycle_view;
    private AdapterCourses mAdapter;
    private int current_step = 1;
    private TextView status, select_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_drawer_enroll);

        initFont();
        initStepper();
        userManager = UserManager.getInstance(getSharedPreferences("user_prefs", MODE_PRIVATE));

        container = findViewById(R.id.container);

        swipe_refresh = findViewById(R.id.swipe_refresh_layout);

        builder = new AlertDialog.Builder(this);
        initToolbar();
        initNavigationMenu();
        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", MODE_PRIVATE));
        service = RetrofitBuilder.createServiceWithAuth(Routes.class, tokenManager);
        dialog = KProgressHUD.create(MenuDrawerEnrollActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.loading))
                .setMaxProgress(100);
        dialog.show();
        recyclerView = findViewById(R.id.card_courses_recycle_view);
        card_sections_recycle_view = findViewById(R.id.card_sections_recycle_view);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        if (userManager.getUser().getAdmin().equals(1)) {
            nav_Menu.findItem(R.id.nav_my_courses).setVisible(false);
            nav_Menu.findItem(R.id.nav_enroll).setVisible(false);
            nav_Menu.findItem(R.id.nav_courses).setVisible(true);
            nav_Menu.findItem(R.id.nav_sections).setVisible(true);
            nav_Menu.findItem(R.id.nav_instructors).setVisible(true);
            nav_Menu.findItem(R.id.nav_students).setVisible(true);
            nav_Menu.findItem(R.id.nav_enrollments).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.nav_my_courses).setVisible(true);
            nav_Menu.findItem(R.id.nav_enroll).setVisible(true);
            nav_Menu.findItem(R.id.nav_courses).setVisible(false);
            nav_Menu.findItem(R.id.nav_sections).setVisible(false);
            nav_Menu.findItem(R.id.nav_instructors).setVisible(false);
            nav_Menu.findItem(R.id.nav_students).setVisible(false);
            nav_Menu.findItem(R.id.nav_enrollments).setVisible(false);
        }
        final List<CourseModel> items = new ArrayList<>();
        Call<Entries<CourseModel>> call = service.getStudentCourses(page, 10, "", "", "");
        call.enqueue(new Callback<Entries<CourseModel>>() {
            @Override
            public void onResponse(Call<Entries<CourseModel>> call, Response<Entries<CourseModel>> response) {
                dialog.dismiss();
                items.addAll(response.body().getEntries());
                //set data and list adapter
                generateCoursesList(items);
//                mAdapter = new AdapterCourses(getApplicationContext(), items);
//                recyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<Entries<CourseModel>> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullAndRefresh();
            }
        });
    }

    private void generateCoursesList(List<CourseModel> courses) {

        AdapterEnrollCourses adapter = new AdapterEnrollCourses(this, courses);

        recyclerView.setLayoutManager(new LinearLayoutManager(MenuDrawerEnrollActivity.this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new AdapterEnrollCourses.OnItemClickListener() {
            @Override
            public void onItemClick(View view, CourseModel obj, int position) {
                course_id = obj.getId();
                nextStep(current_step);
                bottomProgressDots(current_step);
            }
        });

    }

    private void generateSectionsList(List<SectionModel> sections) {

        AdapterEnrollSections adapter = new AdapterEnrollSections(this, sections);

        card_sections_recycle_view.setLayoutManager(new LinearLayoutManager(MenuDrawerEnrollActivity.this, LinearLayoutManager.VERTICAL, false));
        card_sections_recycle_view.setAdapter(adapter);
        adapter.setOnItemClickListener(new AdapterEnrollSections.OnItemClickListener() {
            @Override
            public void onItemClick(View view, SectionModel obj, int position) {
                section_id = obj.getId();
                section_no = obj.getSection_no();
                instructor_id = obj.getInstructor_id();

            }
        });

    }

    private void save() {
        if (section_id == 0) {
            Toast.makeText(getApplicationContext(), "Please select section before.", Toast.LENGTH_LONG).show();
            return;
        }
        dialog.show();
        Call<MessageModel> call = service.enroll(section_id, section_no, instructor_id, course_id);
        call.enqueue(new Callback<MessageModel>() {
            @Override
            public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {
                dialog.dismiss();
                swipeProgress(false);
                startActivity(new Intent(getApplicationContext(), MenuDrawerEnrollActivity.class));
                finish();
            }

            @Override
            public void onFailure(Call<MessageModel> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    private void pullAndRefresh() {
        dialog.show();
        swipeProgress(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeProgress(false);
                initComponent();
            }
        }, 1000);
    }

    public void initComponent() {
        dialog.show();
        final List<CourseModel> items = new ArrayList<>();
        Call<Entries<CourseModel>> call = service.getStudentCourses(page, 10, "", "", "");
        call.enqueue(new Callback<Entries<CourseModel>>() {
            @Override
            public void onResponse(Call<Entries<CourseModel>> call, Response<Entries<CourseModel>> response) {
                dialog.dismiss();
                swipeProgress(false);
                items.addAll(response.body().getEntries());
                generateCoursesList(items);
            }

            @Override
            public void onFailure(Call<Entries<CourseModel>> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }

    public void getSections(int course) {
        dialog.show();
        final List<SectionModel> items = new ArrayList<>();
        Call<List<SectionModel>> call = service.getCourseSections(course);
        call.enqueue(new Callback<List<SectionModel>>() {
            @Override
            public void onResponse(Call<List<SectionModel>> call, Response<List<SectionModel>> response) {
                dialog.dismiss();
                swipeProgress(false);
                items.addAll(response.body());
                generateSectionsList(items);
            }

            @Override
            public void onFailure(Call<List<SectionModel>> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            return;
        }
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
        swipe_refresh.setColorSchemeColors(
                getResources().getColor(android.R.color.holo_blue_bright),
                getResources().getColor(android.R.color.holo_green_light),
                getResources().getColor(android.R.color.holo_orange_light),
                getResources().getColor(android.R.color.holo_red_light)
        );
    }


    private void initFont() {
        FontsOverride.setDefaultFont(this, "SERIF", "fonts/JF-Flat-regular.ttf");
        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/JF-Flat-regular.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/JF-Flat-regular.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/JF-Flat-regular.ttf");
    }

    private void initStepper() {
        status = (TextView) findViewById(R.id.status);
        select_type = (TextView) findViewById(R.id.select_type);

        ((LinearLayout) findViewById(R.id.lyt_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backStep(current_step - 1);
                bottomProgressDots(current_step - 1);
            }
        });

        ((LinearLayout) findViewById(R.id.lyt_next)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (current_step + 1 > MAX_STEP) {
                    save();
                    return;
                }
                nextStep(current_step + 1);
                bottomProgressDots(current_step + 1);
            }
        });

        String str_progress = String.format(getString(R.string.step_of), current_step, MAX_STEP);
        status.setText(str_progress);
        select_type.setText("Select Course");
        bottomProgressDots(current_step);
    }

    private void nextStep(int progress) {
        Toast.makeText(getApplicationContext(), progress + "", Toast.LENGTH_LONG).show();
        if (course_id == 0) {
            Toast.makeText(getApplicationContext(), "Please select course before.", Toast.LENGTH_LONG).show();
            return;
        }
        if (progress < MAX_STEP) {
            progress++;
            current_step = progress;
            ViewAnimation.fadeOutIn(status);
        }
        if (progress == 2) {
            recyclerView.setVisibility(View.GONE);
            card_sections_recycle_view.setVisibility(View.VISIBLE);
            getSections(course_id);
            select_type.setText("Select Section");
        }
        if (progress == 1) {
            recyclerView.setVisibility(View.VISIBLE);
            card_sections_recycle_view.setVisibility(View.GONE);
            initComponent();
            select_type.setText("Select Course");
        }
        if (progress == MAX_STEP) {
            ((TextView) findViewById(R.id.next_btn)).setText("Save");
        } else {
            ((TextView) findViewById(R.id.next_btn)).setText("Next");
        }
        String str_progress = String.format(getString(R.string.step_of), current_step, MAX_STEP);
        status.setText(str_progress);
    }

    private void backStep(int progress) {
        if (progress > 1) {
            progress--;
            current_step = progress;
            ViewAnimation.fadeOutIn(status);
        }
        if (progress == 2) {
            recyclerView.setVisibility(View.GONE);
            card_sections_recycle_view.setVisibility(View.VISIBLE);
            getSections(course_id);
            select_type.setText("Select Section");
        }
        if (progress == 1) {
            recyclerView.setVisibility(View.VISIBLE);
            card_sections_recycle_view.setVisibility(View.GONE);
            initComponent();
            select_type.setText("Select Course");
        }
        String str_progress = String.format(getString(R.string.step_of), current_step, MAX_STEP);
        status.setText(str_progress);
    }

    private void bottomProgressDots(int current_index) {
        current_index--;
        LinearLayout dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        ImageView[] dots = new ImageView[MAX_STEP];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(this);
            int width_height = 15;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle);
            dots[i].setColorFilter(getResources().getColor(R.color.grey_20), PorterDuff.Mode.SRC_IN);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[current_index].setImageResource(R.drawable.shape_circle);
            dots[current_index].setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Enroll");
        Tools.setSystemBarColor(this, R.color.colorPrimary);
    }

    private void initNavigationMenu() {
        final NavigationView nav_view = findViewById(R.id.nav_view);
        drawer = findViewById(R.id.drawer_layout);
        View headerView = nav_view.getHeaderView(0);
        String full_name = userManager.getUser().getFull_name();
        String email = userManager.getUser().getEmail();
        if (full_name != null) {
            drawer_user_name = headerView.findViewById(R.id.drawer_user_name);
            drawer_user_email = headerView.findViewById(R.id.drawer_user_email);
            drawer_user_name.setText(full_name);
            drawer_user_email.setText(email);
        }
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(final MenuItem item) {
                onItemNavigationClicked(item);
                return true;
            }
        });

        // open drawer at start
//        drawer.openDrawer(GravityCompat.START);
    }

    private void onItemNavigationClicked(MenuItem item) {
        if (item.getItemId() == R.id.nav_logout) {
            tokenManager.deleteToken();
            Intent i = new Intent(this.getApplicationContext(), LoginActivity.class);
            startActivity(i);
            finish();
        }
        switch (item.getItemId()) {
            case R.id.nav_courses:
                Intent i = new Intent(this.getApplicationContext(), MenuDrawerCoursesActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.nav_my_courses:
                Intent myi = new Intent(this.getApplicationContext(), MenuDrawerMyCoursesActivity.class);
                startActivity(myi);
                finish();
                break;
            case R.id.nav_enroll:
                Intent enrolli = new Intent(this.getApplicationContext(), MenuDrawerEnrollActivity.class);
                startActivity(enrolli);
                finish();
                break;
            case R.id.nav_logout:
                tokenManager.deleteToken();
                Intent i_logout = new Intent(this.getApplicationContext(), LoginActivity.class);
                startActivity(i_logout);
                finish();
                break;
            case R.id.nav_sections:
                Intent i_nav_sections = new Intent(this.getApplicationContext(), MenuDrawerSectionsActivity.class);
                startActivity(i_nav_sections);
                finish();
                break;
            case R.id.nav_instructors:
                Intent i_nav_instructors = new Intent(this.getApplicationContext(), MenuDrawerInstructorsActivity.class);
                startActivity(i_nav_instructors);
                finish();
                break;
            case R.id.nav_students:
                Intent i_nav_students = new Intent(this.getApplicationContext(), MenuDrawerStudentsActivity.class);
                startActivity(i_nav_students);
                finish();
                break;
            case R.id.nav_enrollments:
                Intent i_nav_enrollments = new Intent(this.getApplicationContext(), MenuDrawerEnrollmentActivity.class);
                startActivity(i_nav_enrollments);
                finish();
                break;
        }
        drawer.closeDrawers();
    }
}