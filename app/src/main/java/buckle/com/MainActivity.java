package buckle.com;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import buckle.com.network.TokenManager;
import buckle.com.network.UserManager;
import buckle.com.utils.Tools;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private ActionBar actionBar;
    private Toolbar toolbar;
    private CardView Courses, Students, Instructors, Sections, Enrollments, MyCourses, Enroll;
    UserManager userManager;
    TokenManager tokenManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolbar();
        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", MODE_PRIVATE));
//        tokenManager.deleteToken();
        userManager = UserManager.getInstance(getSharedPreferences("user_prefs", MODE_PRIVATE));
        if (userManager.getUser().getAdmin().equals(1)) {
            findViewById(R.id.MyCourses).setVisibility(View.GONE);
            findViewById(R.id.Enroll).setVisibility(View.GONE);
            findViewById(R.id.Courses).setVisibility(View.VISIBLE);
            findViewById(R.id.Students).setVisibility(View.VISIBLE);
            findViewById(R.id.Instructors).setVisibility(View.VISIBLE);
            findViewById(R.id.Sections).setVisibility(View.VISIBLE);
            findViewById(R.id.Enrollments).setVisibility(View.VISIBLE);
        }else{
            findViewById(R.id.MyCourses).setVisibility(View.VISIBLE);
            findViewById(R.id.Enroll).setVisibility(View.VISIBLE);
            findViewById(R.id.Courses).setVisibility(View.GONE);
            findViewById(R.id.Students).setVisibility(View.GONE);
            findViewById(R.id.Instructors).setVisibility(View.GONE);
            findViewById(R.id.Sections).setVisibility(View.GONE);
            findViewById(R.id.Enrollments).setVisibility(View.GONE);
        }
        MyCourses = findViewById(R.id.MyCourses);
        MyCourses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MenuDrawerMyCoursesActivity.class));
                finish();
            }
        });

        Enroll = findViewById(R.id.Enroll);
        Enroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MenuDrawerEnrollActivity.class);
                startActivity(i);
            }
        });
        Courses = findViewById(R.id.Courses);
        Courses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MenuDrawerCoursesActivity.class));
                finish();
            }
        });

        Students = findViewById(R.id.Students);
        Students.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MenuDrawerStudentsActivity.class);
                startActivity(i);
            }
        });

        Instructors = findViewById(R.id.Instructors);
        Instructors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MenuDrawerInstructorsActivity.class));
                finish();
            }
        });

        Sections = findViewById(R.id.Sections);
        Sections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MenuDrawerSectionsActivity.class));
                finish();
            }
        });

        Enrollments = findViewById(R.id.Enrollments);
        Enrollments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MenuDrawerEnrollmentActivity.class);
                startActivity(i);
            }
        });
    }

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Home");
        Tools.setSystemBarColor(this, R.color.colorPrimary);
    }

}
