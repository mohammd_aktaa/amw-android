package buckle.com.model;


import java.util.List;
import java.util.Map;

public class MessageModel {

    String message;

    Map<String, List<String>> errors;

    public Map<String, List<String>> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, List<String>> errors) {
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
