package buckle.com.model;


import java.util.List;

public class Entries<T> {

    public List<T> getEntries() {
        return entries;
    }

    public void setEntries(List<T> entries) {
        this.entries = entries;
    }

    List<T> entries;


}