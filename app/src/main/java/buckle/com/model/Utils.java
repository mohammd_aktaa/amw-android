package buckle.com.model;

import java.io.IOException;
import java.lang.annotation.Annotation;

import buckle.com.network.RetrofitBuilder;
import okhttp3.ResponseBody;
import retrofit2.Converter;

public class Utils {
    public static MessageModel converErrors(ResponseBody response){
        Converter<ResponseBody, MessageModel> converter = RetrofitBuilder.getRetrofit().responseBodyConverter(MessageModel.class, new Annotation[0]);

        MessageModel apiError = null;

        try {
            apiError = converter.convert(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return apiError;
    }
}
