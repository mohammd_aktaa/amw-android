package buckle.com.model;


public class SectionModel {
    Integer id;
    Integer section_no;
    Integer room_no;
    String time;
    Integer instructor_id;
    Integer course_id;
    CourseModel course;
    InstructorModel instructor;
    String created_at;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSection_no() {
        return section_no;
    }

    public void setSection_no(Integer section_no) {
        this.section_no = section_no;
    }

    public Integer getRoom_no() {
        return room_no;
    }

    public void setRoom_no(Integer room_no) {
        this.room_no = room_no;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getInstructor_id() {
        return instructor_id;
    }

    public void setInstructor_id(Integer instructor_id) {
        this.instructor_id = instructor_id;
    }

    public Integer getCourse_id() {
        return course_id;
    }

    public void setCourse_id(Integer course_id) {
        this.course_id = course_id;
    }

    public CourseModel getCourse() {
        return course;
    }

    public void setCourse(CourseModel course) {
        this.course = course;
    }

    public InstructorModel getInstructor() {
        return instructor;
    }

    public void setInstructor(InstructorModel instructor) {
        this.instructor = instructor;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


}
