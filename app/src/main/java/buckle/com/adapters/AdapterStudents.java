package buckle.com.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import buckle.com.MenuDrawerStudentsActivity;
import buckle.com.R;
import buckle.com.fragment.StudentDialogFullscreenFragment;
import buckle.com.model.MessageModel;
import buckle.com.model.StudentModel;
import buckle.com.network.RetrofitBuilder;
import buckle.com.network.Routes;
import buckle.com.network.TokenManager;
import buckle.com.utils.ItemAnimation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class AdapterStudents extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int DIALOG_QUEST_CODE = 300;
    AlertDialog.Builder builder;
    Routes service;
    TokenManager tokenManager;
    private List<StudentModel> items = new ArrayList<>();
    private Context context;
    private OnItemClickListener mOnItemClickListener;
    private int lastPosition = -1;
    private boolean on_attach = true;

    public AdapterStudents(Context context, List<StudentModel> items) {
        this.items = items;
        this.context = context;
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_student, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            final OriginalViewHolder view = (OriginalViewHolder) holder;
            tokenManager = TokenManager.getInstance(context.getSharedPreferences("prefs", MODE_PRIVATE));
            service = RetrofitBuilder.createServiceWithAuth(Routes.class, tokenManager);
            final StudentModel student = items.get(position);
            setAnimation(view.itemView, position);
            view.full_name.setText(student.getFull_name());
            view.gender.setText(student.getGender());
            view.address.setText(student.getAddress());
            view.mobile_number.setText(student.getMobile_number());
            view.email.setText(student.getEmail());
            view.username.setText(student.getUsername());
            view.reg_year.setText(student.getReg_year().toString());
            final int p = position;
            builder = new AlertDialog.Builder(context);
            view.delete_Student.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    builder.setMessage("Do you want to delete this Student?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Call<MessageModel> call = service.deleteStudent(student.getId());
                                    call.enqueue(new Callback<MessageModel>() {
                                        @Override
                                        public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {
                                            if (response.isSuccessful()) {
                                                if (mOnItemClickListener != null) {
                                                    mOnItemClickListener.onItemClick(v, items.get(p), p);
                                                }
                                            } else {
                                                if (response.code() == 500) {
                                                      Toast.makeText(context.getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                                                }
                                                if (response.code() == 401) {
                                                      Toast.makeText(context.getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                                                }
                                                if (response.code() == 403) {
                                                      Toast.makeText(context.getApplicationContext(), "Can't Delete Student", Toast.LENGTH_LONG).show();
                                                }
                                            }

                                        }

                                        @Override
                                        public void onFailure(Call<MessageModel> call, Throwable t) {
                                            Toast.makeText(context.getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                                        }
                                    });

                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                }
                            });
                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.setTitle("Delete Student");
                    alert.show();
                }
            });
            view.edit_Student.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    showDialogFullscreen(student);
                }
            });

        }
    }

    private void showDialogFullscreen(StudentModel student) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        StudentDialogFullscreenFragment newFragment = new StudentDialogFullscreenFragment();
        newFragment.setRequestCode(DIALOG_QUEST_CODE);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
        newFragment.setValues(student, "UPDATE");
        newFragment.setOnCallbackResult(new StudentDialogFullscreenFragment.CallbackResult() {
            @Override
            public void sendResult(int requestCode, Object obj) {
                if (requestCode == DIALOG_QUEST_CODE) {
//                    initComponent();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private void setAnimation(View view, int position) {
        if (position > lastPosition) {
            ItemAnimation.animate(view, on_attach ? position : -1, 2);
            lastPosition = position;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, StudentModel obj, int position);
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public TextView full_name, gender, address, mobile_number, email, username, reg_year;
        ImageButton delete_Student, edit_Student;
        CardView card_Student;

        public OriginalViewHolder(View v) {
            super(v);
            full_name = v.findViewById(R.id.full_name);
            gender = v.findViewById(R.id.gender);
            address = v.findViewById(R.id.address);
            mobile_number = v.findViewById(R.id.mobile_number);
            email = v.findViewById(R.id.email);
            username = v.findViewById(R.id.username);
            reg_year = v.findViewById(R.id.reg_year);
            delete_Student = v.findViewById(R.id.delete_Student);
            edit_Student = v.findViewById(R.id.edit_Student);
//            card_Student = v.findViewById(R.id.card_Student);
        }
    }
}