package buckle.com.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import buckle.com.R;
import buckle.com.model.EnrollmentModel;
import buckle.com.network.RetrofitBuilder;
import buckle.com.network.Routes;
import buckle.com.network.TokenManager;
import buckle.com.utils.ItemAnimation;

import static android.content.Context.MODE_PRIVATE;

public class AdapterEnrollment extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int DIALOG_QUEST_CODE = 300;
    AlertDialog.Builder builder;
    Routes service;
    TokenManager tokenManager;
    private List<EnrollmentModel> items = new ArrayList<>();
    private Context context;
    private OnItemClickListener mOnItemClickListener;
    private int lastPosition = -1;
    private boolean on_attach = true;

    public AdapterEnrollment(Context context, List<EnrollmentModel> items) {
        this.items = items;
        this.context = context;
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_enrollment, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            final OriginalViewHolder view = (OriginalViewHolder) holder;
            tokenManager = TokenManager.getInstance(context.getSharedPreferences("prefs", MODE_PRIVATE));
            service = RetrofitBuilder.createServiceWithAuth(Routes.class, tokenManager);
            final EnrollmentModel enrollment = items.get(position);
            setAnimation(view.itemView, position);
            view.full_name.setText(enrollment.getStudent().getFull_name());
            view.course.setText(enrollment.getCourse().getTitle());
            view.instructor.setText(enrollment.getSection().getInstructor().getFull_name());
            view.section.setText(enrollment.getSection().getSection_no().toString());
            view.grade.setText(enrollment.getGrade().toString());
        }
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    private void setAnimation(View view, int position) {
        if (position > lastPosition) {
            ItemAnimation.animate(view, on_attach ? position : -1, 2);
            lastPosition = position;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, EnrollmentModel obj, int position);
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public TextView full_name, course, instructor, section, grade;

        public OriginalViewHolder(View v) {
            super(v);
            full_name = v.findViewById(R.id.full_name);
            course = v.findViewById(R.id.course);
            instructor = v.findViewById(R.id.instructor);
            section = v.findViewById(R.id.section);
            grade = v.findViewById(R.id.grade);
        }
    }
}