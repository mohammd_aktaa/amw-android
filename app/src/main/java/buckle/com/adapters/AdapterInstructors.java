package buckle.com.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import buckle.com.R;
import buckle.com.fragment.InstructorDialogFullscreenFragment;
import buckle.com.model.InstructorModel;
import buckle.com.model.MessageModel;
import buckle.com.network.RetrofitBuilder;
import buckle.com.network.Routes;
import buckle.com.network.TokenManager;
import buckle.com.utils.ItemAnimation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class AdapterInstructors extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int DIALOG_QUEST_CODE = 300;
    AlertDialog.Builder builder;
    Routes service;
    TokenManager tokenManager;
    private List<InstructorModel> items = new ArrayList<>();
    private Context context;
    private OnItemClickListener mOnItemClickListener;
    private int lastPosition = -1;
    private boolean on_attach = true;

    public AdapterInstructors(Context context, List<InstructorModel> items) {
        this.items = items;
        this.context = context;
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_instructor, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            final OriginalViewHolder view = (OriginalViewHolder) holder;
            tokenManager = TokenManager.getInstance(context.getSharedPreferences("prefs", MODE_PRIVATE));
            service = RetrofitBuilder.createServiceWithAuth(Routes.class, tokenManager);
            final InstructorModel instructor = items.get(position);
            setAnimation(view.itemView, position);
            view.full_name.setText(instructor.getFull_name());
            view.gender.setText(instructor.getGender());
            view.address.setText(instructor.getAddress());
            view.mobile_number.setText(instructor.getMobile_number());
            final int p = position;
            builder = new AlertDialog.Builder(context);
            view.delete_Instructor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    builder.setMessage("Do you want to delete this Instructor?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Call<MessageModel> call = service.deleteInstructor(instructor.getId());
                                    call.enqueue(new Callback<MessageModel>() {
                                        @Override
                                        public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {
                                            if (response.isSuccessful()) {
                                                if (mOnItemClickListener != null) {
                                                    mOnItemClickListener.onItemClick(v, items.get(p), p);
                                                }
                                            } else {
                                                if (response.code() == 500) {
                                                    Toast.makeText(context.getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                                                }
                                                if (response.code() == 401) {
                                                    Toast.makeText(context.getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                                                }
                                                if (response.code() == 403) {
                                                    Toast.makeText(context.getApplicationContext(), "Can't Delete Instructor", Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<MessageModel> call, Throwable t) {
                                            Toast.makeText(context.getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                                        }
                                    });

                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                }
                            });
                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.setTitle("Delete Instructor");
                    alert.show();
                }
            });
            view.edit_Instructor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    showDialogFullscreen(instructor);
                }
            });

        }
    }

    private void showDialogFullscreen(InstructorModel instructor) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        InstructorDialogFullscreenFragment newFragment = new InstructorDialogFullscreenFragment();
        newFragment.setRequestCode(DIALOG_QUEST_CODE);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
        newFragment.setValues(instructor, "UPDATE");
        newFragment.setOnCallbackResult(new InstructorDialogFullscreenFragment.CallbackResult() {
            @Override
            public void sendResult(int requestCode, Object obj) {
                if (requestCode == DIALOG_QUEST_CODE) {
//                    initComponent();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private void setAnimation(View view, int position) {
        if (position > lastPosition) {
            ItemAnimation.animate(view, on_attach ? position : -1, 2);
            lastPosition = position;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, InstructorModel obj, int position);
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public TextView full_name, gender, address, mobile_number;
        ImageButton delete_Instructor, edit_Instructor;
        CardView card_Instructor;

        public OriginalViewHolder(View v) {
            super(v);
            full_name = v.findViewById(R.id.full_name);
            gender = v.findViewById(R.id.gender);
            address = v.findViewById(R.id.address);
            mobile_number = v.findViewById(R.id.mobile_number);
            delete_Instructor = v.findViewById(R.id.delete_Instructor);
            edit_Instructor = v.findViewById(R.id.edit_Instructor);
//            card_Instructor = v.findViewById(R.id.card_Instructor);
        }
    }
}