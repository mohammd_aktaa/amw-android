package buckle.com.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import buckle.com.R;
import buckle.com.model.CourseModel;
import buckle.com.network.RetrofitBuilder;
import buckle.com.network.Routes;
import buckle.com.network.TokenManager;
import buckle.com.utils.ItemAnimation;

import static android.content.Context.MODE_PRIVATE;

public class AdapterEnrollCourses extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int DIALOG_QUEST_CODE = 300;
    AlertDialog.Builder builder;
    Routes service;
    TokenManager tokenManager;
    private List<CourseModel> items = new ArrayList<>();
    private Context context;
    private OnItemClickListener mOnItemClickListener;
    private int lastPosition = -1;
    private boolean on_attach = true;

    public AdapterEnrollCourses(Context context, List<CourseModel> items) {
        this.items = items;
        this.context = context;
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_enroll_course, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            final OriginalViewHolder view = (OriginalViewHolder) holder;
            tokenManager = TokenManager.getInstance(context.getSharedPreferences("prefs", MODE_PRIVATE));
            service = RetrofitBuilder.createServiceWithAuth(Routes.class, tokenManager);
            final CourseModel course = items.get(position);
            setAnimation(view.itemView, position);
            view.course_title.setText(course.getTitle());
            view.course_hours.setText(course.getHours().toString());
            final int p = position;
            view.card_course.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(v, course, p);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private void setAnimation(View view, int position) {
        if (position > lastPosition) {
            ItemAnimation.animate(view, on_attach ? position : -1, 2);
            lastPosition = position;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, CourseModel obj, int position);
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public TextView course_title, course_hours;
        ImageButton delete_course, edit_course;
        CardView card_course;

        public OriginalViewHolder(View v) {
            super(v);
            course_title = v.findViewById(R.id.course_title);
            course_hours = v.findViewById(R.id.course_hours);
            card_course = v.findViewById(R.id.card_course);
        }
    }
}